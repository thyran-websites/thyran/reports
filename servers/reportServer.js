const WebSocketServer = require("websocket").server;
const WebSocketRouter = require("websocket").router;
const Report = require("../classes/Report");
const User = require("../classes/User");

let acceptReportRequest = (request, originAllowenceFunction, serverName) => {
    let getReportNameFromRequestUri = (uri) => {
        let name = uri.substring(1);
        let slashIndex = name.indexOf("/");

        if (slashIndex >= 0)
            name = name.substring(0, slashIndex);

        return name;
    };

    if (!originAllowenceFunction(request.origin)) {
        request.reject(403, "Origin is not allowed");
        return;
    }

    let reportName = getReportNameFromRequestUri(request.resource);
    let report = Report.getFromID(reportName);

    if (!report) {
        request.reject(404, "The requested report could not be found");
        return;
    } else if (!report.isRunning()) {
        request.reject(500, "The requested report is not running");
        return;
    }

    request.on("requestAccepted", (connection) => {
        connection.reportName = reportName;

        report.addConnection(connection);
        report.sendInitialData(connection);

        console.log("ACC connection on server '" + serverName + "' from origin '" + request.origin + "' for report '" + report.getID() + "'");
    });

    request.on("requestRejected", () => {
        console.warn("REJ connection on server '" + serverName + "' from origin '" + request.origin + "'");
    });

    let connection = request.accept(request.origin);
    connection.enabled = false;

    connection.on("message", (data) => {
        let msg = JSON.parse(data.utf8Data);

        if (msg.topic === "ping") {
            let duration = Date.now() - msg.timestamp;

            connection.send(JSON.stringify({
                topic:"pong",
                duration: duration
            }));

            return;
        }

        if (connection.enabled) {
            console.log(msg);
            switch(msg.topic) {
                default:
                    console.log("Error in report connection: the topic '" + msg.topic + "' was not found");
                    break;
            }
        } else if (msg.topic === "enable" && msg.data)
            connection.enabled = true;
    });

    connection.on("close", (reason, description) => {
        let reportName = connection.reportName;
        let report = Report.getFromID(reportName);

        report.removeConnection(connection);

        console.log("CLS connection because '" + description + "' (" + reason + ")");
    });
};

let acceptUserRequest = (request, originAllowenceFunction, serverName) => {
    let getSessionIDFromRequestUri = (uri) => {
        let name = uri.substring(1);
        let slashIndex = name.indexOf("/");

        if (slashIndex >= 0)
            name = name.substring(0, slashIndex);

        return name;
    };

    if (!originAllowenceFunction(request.origin)) {
        request.reject(403, "Origin is not allowed");
        return;
    }

    let sessionID = getSessionIDFromRequestUri(request.resource);
    let user = User.getFromSessionID(sessionID);

    if (!user) {
        request.reject(404, "User is not logged in");
        return;
    }

    request.on("requestAccepted", (connection) => {
        connection.sessionID = sessionID;

        User.getFromSessionID(connection.sessionID).setConnection(connection);
    });

    let connection = request.accept(request.origin);

    connection.on("message", (msg) => {
        let data = JSON.parse(msg.utf8Data);
        let user = User.getFromSessionID(connection.sessionID);

        switch(data.topic) {
            case "user_lifetime":
                if (user)
                    connection.send(JSON.stringify({
                        topic: data.topic,
                        lifetime: user.getLifetime()
                    }));
                break;
            case "user_maxLifetime":
                if (user)
                    connection.send(JSON.stringify({
                        topic: "user_lifetime",
                        lifetime: user.getMaxAge()
                    }));
                break;
            case "refresh_user":
                user.extendLifetime();
                break;
            default:
                break;
        }
    });
};

module.exports = (httpServer, originAllowenceFunction, serverName) => {
    let server = new WebSocketServer({
        httpServer: httpServer,
        autoAcceptConnections: false
    });

    let router = new WebSocketRouter();
    router.attachServer(server);

    router.mount("*", "report", (request) => {
        acceptReportRequest(request, originAllowenceFunction, serverName);
    });

    router.mount("*", "user", (request) => {
        acceptUserRequest(request, originAllowenceFunction, serverName);
    });

    return server;
};