const WebSocketServer = require("websocket").server;
const Endpoint = require("../classes/Endpoint");

let acceptRequest = (request, originAllowenceFunction, serverName) => {
    let getEndpointNameFromRequestUri = (uri) => {
        let name = uri.substring(1);
        let slashIndex = name.indexOf("/");

        if (slashIndex >= 0)
            name = name.substring(0, slashIndex);

        return name;
    }

    if (!originAllowenceFunction(request.origin)) {
        request.reject(403, "Origin is not allowed");

        return;
    }

    let endpointName = getEndpointNameFromRequestUri(request.resource);
    let endpoint = Endpoint.getFromID(endpointName);

    if (!endpoint) {
        request.reject(404, "Endpoint does not exists");

        return;
    } else if (!endpoint.isRunning()) {
        request.reject(500, "Endpoint is not active");

        return;
    }

    request.on("requestAccepted", (connection) => {
        connection.endpointName = endpointName;
    });

    request.accept(null, request.origin);
};

let connect = (connection) => {
    connection.on("message", (data) => {
        let msg = JSON.parse(data.utf8Data);
        
        Endpoint.getFromID(connection.endpointName).input(msg);
    });
}

module.exports = (httpServer, originAllowenceFunction, serverName) => {
    let server = new WebSocketServer({
        httpServer: httpServer,
        autoAcceptConnections: false
    });

    server.on("request", (request) => {
        acceptRequest(request, originAllowenceFunction, serverName);
    });

    server.on("connect", connect);

    return server;
};