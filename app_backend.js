const express = require("express");
const bcrypt = require("bcrypt");

module.exports = (() => {
    let app = express.Router();

    app.use(express.static(__basedir + "/websites/dist/backend"));

    app.get("/generatePassword", (req, res) => {
        if ("password" in req.query)
            bcrypt.hash(req.query.password, hashSaltRounds, (err, pass) => {
                if (err) {
                    res.sendStatus(500);
                    return;
                }

                res.send(pass);
            });
        else
            res.sendStatus(400);
    });

    // TODO enhance backend functionality
    app.post("/test", (req, res) => {
        res.sendStatus(200);
    });

    return app;
})();