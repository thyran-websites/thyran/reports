Select
    `report`.`system_id` as `report`
from
    `group_report`
    left join `report` on `group_report`.`report_id` = `report`.`id`
where
    `group_id` = ?;