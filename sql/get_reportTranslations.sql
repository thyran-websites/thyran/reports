Select
    *
from
    `report_endpoint`
    left join `endpoint` on `report_endpoint`.`endpoint_id` = `endpoint`.`id`
where
    `report_id` = ?
order by
    `endpoint_id`
asc;