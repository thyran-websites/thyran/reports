-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.4.13-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Exportiere Datenbank Struktur für reports
CREATE DATABASE IF NOT EXISTS `reports` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci */;
USE `reports`;

-- Exportiere Struktur von Tabelle reports.endpoint
CREATE TABLE IF NOT EXISTS `endpoint` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `system_id` varchar(50) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `system_id` (`system_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.endpoint: ~0 rows (ungefähr)
DELETE FROM `endpoint`;
/*!40000 ALTER TABLE `endpoint` DISABLE KEYS */;
INSERT INTO `endpoint` (`id`, `system_id`, `active`) VALUES
	(1, 'test', 1);
/*!40000 ALTER TABLE `endpoint` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.endpoint_input
CREATE TABLE IF NOT EXISTS `endpoint_input` (
  `endpoint_id` int(10) unsigned NOT NULL,
  `input_id` varchar(50) NOT NULL,
  PRIMARY KEY (`endpoint_id`,`input_id`),
  KEY `FK_ENDPOINTINPUT_EID` (`endpoint_id`),
  CONSTRAINT `FK_ENDPOINTINPUT_EID` FOREIGN KEY (`endpoint_id`) REFERENCES `endpoint` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.endpoint_input: ~5 rows (ungefähr)
DELETE FROM `endpoint_input`;
/*!40000 ALTER TABLE `endpoint_input` DISABLE KEYS */;
INSERT INTO `endpoint_input` (`endpoint_id`, `input_id`) VALUES
	(1, '1'),
	(1, '2'),
	(1, '3'),
	(1, '4'),
	(1, '5');
/*!40000 ALTER TABLE `endpoint_input` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.group
CREATE TABLE IF NOT EXISTS `group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.group: ~0 rows (ungefähr)
DELETE FROM `group`;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` (`id`, `name`) VALUES
	(1, 'test');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.group_report
CREATE TABLE IF NOT EXISTS `group_report` (
  `group_id` int(10) unsigned NOT NULL,
  `report_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`report_id`),
  KEY `FK_GROUPREPORT_RID` (`report_id`),
  KEY `FK_GROUPREPORT_GID` (`group_id`),
  CONSTRAINT `FK_GROUPREPORT_GID` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_GROUPREPORT_RID` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.group_report: ~0 rows (ungefähr)
DELETE FROM `group_report`;
/*!40000 ALTER TABLE `group_report` DISABLE KEYS */;
INSERT INTO `group_report` (`group_id`, `report_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `group_report` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.report
CREATE TABLE IF NOT EXISTS `report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system_id` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemID` (`system_id`) USING BTREE,
  KEY `active` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.report: ~2 rows (ungefähr)
DELETE FROM `report`;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` (`id`, `system_id`, `name`, `description`, `active`) VALUES
	(1, 'test', 'Test', 'Test Report', 1),
	(2, 'test2', 'Test2', NULL, 1);
/*!40000 ALTER TABLE `report` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.report_endpoint
CREATE TABLE IF NOT EXISTS `report_endpoint` (
  `endpoint_id` int(10) unsigned NOT NULL,
  `endpoint_input_id` varchar(50) NOT NULL,
  `report_id` int(10) unsigned NOT NULL,
  `report_field_id` varchar(50) NOT NULL,
  UNIQUE KEY `endpoint_id_endpoint_input_id_report_id_report_field_id` (`endpoint_id`,`endpoint_input_id`,`report_id`,`report_field_id`),
  KEY `FK_REPORTENDPOINT_RID` (`report_id`),
  KEY `FK_REPORTENDPOINT_EID` (`endpoint_id`),
  CONSTRAINT `FK_REPORTENDPOINT_EID` FOREIGN KEY (`endpoint_id`) REFERENCES `endpoint` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_REPORTENDPOINT_RID` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.report_endpoint: ~5 rows (ungefähr)
DELETE FROM `report_endpoint`;
/*!40000 ALTER TABLE `report_endpoint` DISABLE KEYS */;
INSERT INTO `report_endpoint` (`endpoint_id`, `endpoint_input_id`, `report_id`, `report_field_id`) VALUES
	(1, '1', 1, '1'),
	(1, '2', 1, '2'),
	(1, '3', 1, '3'),
	(1, '4', 1, '4'),
	(1, '5', 1, '7');
/*!40000 ALTER TABLE `report_endpoint` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.report_field
CREATE TABLE IF NOT EXISTS `report_field` (
  `report_id` int(10) unsigned NOT NULL,
  `field_id` varchar(50) NOT NULL,
  `type` enum('INPUT','SUM','PERCENT') NOT NULL,
  `config` longtext DEFAULT NULL,
  `value` longtext DEFAULT NULL,
  PRIMARY KEY (`report_id`,`field_id`),
  KEY `FK_REPORTFIELD_RID` (`report_id`),
  KEY `type` (`type`),
  CONSTRAINT `FK_REPORTFIELD_RID` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.report_field: ~7 rows (ungefähr)
DELETE FROM `report_field`;
/*!40000 ALTER TABLE `report_field` DISABLE KEYS */;
INSERT INTO `report_field` (`report_id`, `field_id`, `type`, `config`, `value`) VALUES
	(1, '1', 'INPUT', '{"inputType":2}', NULL),
	(1, '2', 'INPUT', '{"inputType":2}', NULL),
	(1, '3', 'INPUT', '{"inputType":2}', NULL),
	(1, '4', 'INPUT', '{"inputType":2}', NULL),
	(1, '5', 'SUM', '{"fields":["1","2","3","4"]}', NULL),
	(1, '6', 'PERCENT', '{"percentBase":"5","percentValue":"4"}', NULL),
	(1, '7', 'INPUT', '{"inputType":2}', NULL);
/*!40000 ALTER TABLE `report_field` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.report_tile
CREATE TABLE IF NOT EXISTS `report_tile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` int(10) unsigned NOT NULL,
  `row_index` int(10) unsigned NOT NULL,
  `tile_index` int(10) unsigned NOT NULL,
  `tile_id` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `layout` longtext DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `report_id_tile_id` (`report_id`,`tile_id`) USING BTREE,
  KEY `report_id` (`report_id`),
  KEY `row_index_tile_index` (`row_index`,`tile_index`),
  CONSTRAINT `FK_REPORTTILE_RID` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.report_tile: ~4 rows (ungefähr)
DELETE FROM `report_tile`;
/*!40000 ALTER TABLE `report_tile` DISABLE KEYS */;
INSERT INTO `report_tile` (`id`, `report_id`, `row_index`, `tile_index`, `tile_id`, `name`, `layout`) VALUES
	(1, 1, 0, 0, '09732ce4-8ca6-4388-8f07-aa317cd5dd1f', 'Test1', '{"type": "table",\r\n                        "id": null,\r\n                        "value": {\r\n                            "head": [\r\n                                [\r\n                                    {\r\n                                        "id": null,\r\n                                        "type": "text",\r\n                                        "value": "Name"\r\n                                    },\r\n                                    {\r\n                                        "id": null,\r\n                                        "type": "text",\r\n                                        "value": "Value"\r\n                                    }\r\n                                ]\r\n                            ],\r\n                            "body": [\r\n                                [\r\n                                    {\r\n                                        "id": null,\r\n                                        "type": "text",\r\n                                        "value": "Test"\r\n                                    },\r\n                                    {\r\n                                        "id": 1,\r\n                                        "type": "text",\r\n                                        "value": null\r\n                                    }\r\n                                ],\r\n                                [\r\n                                    {\r\n                                        "id": null,\r\n                                        "type": "text",\r\n                                        "value": "Test"\r\n                                    },\r\n                                    {\r\n                                        "id": 2,\r\n                                        "type": "number",\r\n                                        "value": null\r\n                                    }\r\n                                ],\r\n                                [\r\n                                    {\r\n                                        "id": null,\r\n                                        "type": "text",\r\n                                        "value": "Test"\r\n                                    },\r\n                                    {\r\n                                        "id": 3,\r\n                                        "type": "currency",\r\n                                        "value": null\r\n                                    }\r\n                                ],\r\n                                [\r\n                                    {\r\n                                        "id": null,\r\n                                        "type": "text",\r\n                                        "value": "Test"\r\n                                    },\r\n                                    {\r\n                                        "id": 7,\r\n                                        "type": "currency",\r\n                                        "value": null\r\n                                    }\r\n                                ]\r\n                            ],\r\n                            "foot": []\r\n                        }\r\n                    }'),
	(2, 1, 0, 1, 'b10e6f69-4bf7-4dc7-8dd5-bf19d6e4b696', 'Test2', '{"type": "number",\r\n                        "id": 4,\r\n                        "value": null}'),
	(3, 1, 1, 0, 'a189a6a9-654a-4677-9498-6e163a3913d9', 'Test3', '{"type": "number",\r\n                        "id": 5,\r\n                        "value": null}'),
	(4, 1, 1, 1, 'b63cd5cc-5e62-4e1c-8632-e8962e34bf65', 'Test4', '{"type": "number",\r\n                        "id": 6,\r\n                        "value": null}');
/*!40000 ALTER TABLE `report_tile` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` text DEFAULT NULL,
  `maxAge` int(10) unsigned NOT NULL DEFAULT 300000,
  `requestErrorLifetime` int(11) unsigned NOT NULL DEFAULT 2500,
  `numberDigits` tinyint(3) unsigned NOT NULL,
  `lastReport_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`),
  KEY `FK_USER_LRID` (`lastReport_id`),
  CONSTRAINT `FK_USER_LRID` FOREIGN KEY (`lastReport_id`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.user: ~7 rows (ungefähr)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `maxAge`, `requestErrorLifetime`, `numberDigits`, `lastReport_id`) VALUES
	(1, 'test', '$2b$10$pEKmX2jcXQ.STyd11aQ6oO542v8kGUDcfobD.eXdjNpHdDQsnkLG6', 1200000, 1500, 2, 1),
	(5, 'test2', '$2b$10$J.Dmr2O3j4HZfQofhJ52Wu8ncsD8jK5YJZg3cbuU6hLkvTUEpaf9a', 300000, 2500, 0, 1),
	(6, 'test3', '$2b$10$bf6QSZ2BLp1PYUqQX5sf9ec190weCsP8pegx/fDu0gO5hklLarKAu', 60000, 2500, 0, 1),
	(10, 'test4', '$2b$10$sJHCorQwfqn09VUHLklSfecmtAG5ILMEyyp1DitB.mom48pHIPb1O', 300000, 2500, 0, NULL),
	(11, 'test5', '$2b$10$u3p.ObcscIjOCD8nNF64fOmbM0paxrYr3x6LOxcY0fVlazWnvvlh6', 300000, 2500, 0, NULL),
	(12, 'test6', '$2b$10$eSpqMjNckm8TtaTSPis1cuWY7HZ/mqKb3AzWW4w7F.He/Ww9VDn3C', 300000, 2500, 0, NULL),
	(13, 'test7', '$2b$10$OMsI5yV5wF8HF2X4bJG3deS.VppdP1X5JuOTo7xxf9iIIGhXRfnAK', 300000, 2500, 0, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.user_group
CREATE TABLE IF NOT EXISTS `user_group` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `FK_USERGROUP_UID` (`user_id`),
  KEY `FK_USERGROUP_GID` (`group_id`),
  CONSTRAINT `FK_USERGROUP_GID` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_USERGROUP_UID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.user_group: ~0 rows (ungefähr)
DELETE FROM `user_group`;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` (`user_id`, `group_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle reports.user_report_data
CREATE TABLE IF NOT EXISTS `user_report_data` (
  `user_id` int(11) unsigned NOT NULL,
  `report_tile_id` int(11) unsigned NOT NULL,
  `color` varchar(50) DEFAULT '',
  `name` varchar(50) DEFAULT '',
  PRIMARY KEY (`user_id`,`report_tile_id`),
  KEY `FK_USERREPORTDATA_TID` (`report_tile_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `FK_USERREPORTDATA_TID` FOREIGN KEY (`report_tile_id`) REFERENCES `report_tile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_USERREPORTDATA_UID` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle reports.user_report_data: ~0 rows (ungefähr)
DELETE FROM `user_report_data`;
/*!40000 ALTER TABLE `user_report_data` DISABLE KEYS */;
INSERT INTO `user_report_data` (`user_id`, `report_tile_id`, `color`, `name`) VALUES
	(1, 1, '#fff', 'Test');
/*!40000 ALTER TABLE `user_report_data` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
