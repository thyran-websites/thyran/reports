Select
    `user`.`id`,
    `user`.`password`,
    `user`.`maxAge`,
    `user`.`requestErrorLifetime`,
    `user`.`numberDigits`,
    `report`.`system_id` as `report`
from
    `user`
    left join `report` on `user`.`lastReport_id` = `report`.`id`
where
    `username` = ?;