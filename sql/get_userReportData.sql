Select
    `report_tile`.`tile_id`,
    `user_report_data`.`color`,
    `user_report_data`.`name`
from
    `user_report_data`
    left join `report_tile` on `user_report_data`.`report_tile_id` = `report_tile`.`id`
where
    `user_id` = ?
    and `report_tile`.`report_id` = ?;