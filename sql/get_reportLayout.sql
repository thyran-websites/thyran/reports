Select
    *
from
    `report_tile`
where
    `report_id` = ?
order by
    `row_index`,
    `tile_index`
asc;