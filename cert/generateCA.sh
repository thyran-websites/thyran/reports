read -p "Enter key  file name: " keyFileName
read -p "Enter cert file name: " certFileName

keyFileName="$keyFileName.key"
certFileName="$certFileName.cert"

openssl genrsa -out $keyFileName 4096
openssl req -x509 -new -nodes -extensions v3_ca -key $keyFileName -days 365 -out $certFileName -sha512

read