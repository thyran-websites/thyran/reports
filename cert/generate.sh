#!/bin/bash

read -p "Enter key  file name: " keyFileName
read -p "Enter cert file name: " certFileName

keyFileName="$keyFileName.key"
certFileName="$certFileName.cert"

read -p "Enter key length [2^x]: " keyLength

read -p "Enter root CA key  file name: " caKeyFileName
read -p "Enter root CA cert file name: " caCertFileName

caKeyFileName="$caKeyFileName.key"
caCertFileName="$caCertFileName.cert"

openssl genrsa -out $keyFileName $keyLength
openssl req -new -key $keyFileName -out tmp.csr -sha512
openssl x509 -req -in tmp.csr -CA $caCertFileName -CAkey $caKeyFileName -CAcreateserial -out $certFileName -sha512

rm tmp.csr