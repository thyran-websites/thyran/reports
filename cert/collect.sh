#!/bin/bash

read -p "Enter key  file name: " keyFileName
read -p "Enter cert file name: " certFileName

read -p "Enter password for certificate: " password

pfxFileName="$certFileName.pfx"
keyFileName="$keyFileName.key"
certFileName="$certFileName.cert"

openssl pkcs12 -export -out $pfxFileName -inkey $keyFileName -in $certFileName -passout pass:$password