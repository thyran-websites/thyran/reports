let requestError = (error) => {
    const requestErrorLivetimeFallback = 1500;
    
    let errorHandle = $("#report-errors");

    let newError = $("<div class='report-error'>");
    newError.on("transitionend", (e) => {
        if (e.originalEvent.propertyName === "height" && !newError.hasClass("show"))
            newError.remove();
    });

    let errorText = $(
        "<span class='error-text'>" +
        error.method.toUpperCase() +
        " <a class='error-link'>" + error.url + "</a>" +
        " <a class='error-code'>" + error.code + "</a>" +
        " (<a class='error-reason'>" + error.text + "</a>)</span>"
    );

    newError.append(errorText);
    errorHandle.append(newError);

    setTimeout(() => {
        newError.addClass("show");

        setTimeout(() => {
            newError.removeClass("show");
        }, !userConfig.requestErrorLivetime ? requestErrorLivetimeFallback : userConfig.requestErrorLivetime);
    }, 25);
};