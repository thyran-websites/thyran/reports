let global = {
    SESSION_ID: null
};

let userConfig = {
    reportHost: null,
    lastReport: null,
    requestErrorLivetime: null,
    numberDigitRound: 2
};

let tileColorPicker = null;

const pingIntervalFallback = 5000;

let main = $("#report");
let reportStatus = $("#report-status");
let connectionStatus = $("#connection-status");
let pingStatus = connectionStatus.find("[report-prop='ping']");
let lifetimeStatus = $("#lifetime-status");
let lifetimeSecondStatus = lifetimeStatus.find("[report-prop='lifetime-seconds']");
let lifetimeMinuteStatus = lifetimeStatus.find("[report-prop='lifetime-minutes']");

let wsClient = null;
let worker = null;

let SESSION_HEADER = () => { return { "User-Session-ID": global.SESSION_ID }; };

let changeColorOnTile = (color) => {
    let tileSlide = $("#tile-edit-slide");
    if (tileSlide.is("[tile-id]")) {
        let tile = main.find(".report-tile[tile-id='" + tileSlide.attr("tile-id") + "']").find(".tile-head").css("background", color.hex);
        tile.trigger("save");
    }
};

let toggleSlideMenu = (target = null, mode = null) => {
    $(".slide-menu").each(function() {
        let self = $(this);
        let id = self.prop("id");
        let slideMenuCallback = getSlideMenuCallbacks()[id];

        let open = (menu, callback, force = false) => {
            let toggleMenu = (force) => {
                let wasHidden = !menu.hasClass("show");

                if (force)
                    menu.addClass("show");
                else
                    menu.toggleClass("show");

                if (menu.hasClass("show") && wasHidden)
                    menu.trigger("onshow");
                if (!menu.hasClass("show") && !wasHidden)
                    menu.trigger("onhide");
            };

            if (callback && callback.hasOwnProperty("show") && typeof callback.show === typeof Function)
                callback.show(() => {
                    toggleMenu(force);
                });
            else
                toggleMenu(force);
        };

        let close = (menu, callback, force = false) => {
            let closeMenu = (force) => {
                let wasHidden = !menu.hasClass("show");

                menu.removeClass("show");

                if (!menu.hasClass("show") && !wasHidden)
                    menu.trigger("onhide");
            };

            if (callback && callback.hasOwnProperty("hide") && typeof callback.hide === typeof Function)
                callback.hide(() => {
                    closeMenu(force);
                });
            else
                closeMenu(force);
        };

        
        if (!target)
            close(self, slideMenuCallback);
        else if (target === "#" + id) {
            if (mode === "show")
                open(self, slideMenuCallback, true);
            else if (mode === "hide")
                close(self, slideMenuCallback);
            else {
                if (self.hasClass("show"))
                    close(self, slideMenuCallback);
                else
                    open(self, slideMenuCallback);
            }
        } else
            close(self, slideMenuCallback);
    });
}

let showUserMenu = (menu = null) => {
    if (menu)
        $(".user-menu").each(function() {
            if (menu === "#" + $(this).prop("id"))
                $(this).attr("hidden", false);
            else
                $(this).attr("hidden", true);
        });
    else
        $(".user-menu").attr("hidden", true);
};

let tryConnect = () => {
    let reportHost = userConfig.reportHost;
    if (reportHost) {
        let report = userConfig.lastReport;
        if (report) {
            let data = {
                report: report
            };

            let options = {
                url: "/isAuthorized",
                type: "POST",
                dataType: "json",
                data: data,
                headers: SESSION_HEADER()
            };

            $.ajax(options)
            .done((result) => {
                let reportName = result.name;
                reportStatus.find("[report-prop='name']").text(reportName);

                connectToReport(reportHost, report, result.data);
            })
            .fail((error) => {
                connectionError({
                    main: "Not Authorized",
                    sub: "The requested report is not available"
                });

                requestError({
                    method: options.type,
                    url: options.url,
                    code: error.status,
                    text: error.statusText
                });
            });
        } else {
            connectionError({
                main: "Report Error",
                sub: "No report in config"
            });

            toggleSlideMenu("#report-slide");
        }
    } else
        connectionError({
            main: "Report Error",
            sub: "No reportHost in config"
        });

    if (worker)
        worker.terminate();

    // load the service worker if the user is logged in
    worker = new Worker("/userWorker");
    worker.onmessage = (msg) => {
        let data = JSON.parse(msg.data);

        switch(data.type) {
            case "login":
                if (!data.status) {
                    connectionError({
                        main: "Authorization Error",
                        sub: "User was logged out"
                    });

                    reportStatus.find("[report-prop='name']").text("");
                    pingStatus.attr("speed-index", "no");
                    pingStatus.text("-");

                    lifetimeStatus.removeClass("short-time");
                    lifetimeMinuteStatus.text("00");
                    lifetimeSecondStatus.text("00");

                    if (wsClient)
                        wsClient.terminate();

                    showUserMenu("#user-login");
                    toggleSlideMenu("#user-slide", "show");
                }
                break;
            case "connection":
                if (!data.status) {
                    connectionError({
                        main: "Connection Error",
                        sub: "No connection to the server"
                    });
                }
                break;
            case "refresh":
                if (data.status)
                    location.reload();

                break;
            case "user_lifetime":
                let time = data.lifetime;
                let seconds = time / 1000;
                let minutes = seconds / 60;

                let displayMinutes = Math.floor(minutes);
                let displaySeconds = Math.floor(seconds - displayMinutes * 60);

                lifetimeStatus.toggleClass("short-time", seconds < 30);
                lifetimeMinuteStatus.text(displayMinutes < 10 ? "0" + displayMinutes : displayMinutes);
                lifetimeSecondStatus.text(displaySeconds < 10 ? "0" + displaySeconds : displaySeconds);
                break;
            default:
                break;
        }
    };
    worker.postMessage(JSON.stringify({
        start: true,
        sessionID: global.SESSION_ID
    }));
}

let connectionError = (message) => {
    main.html("");

    let noReport = $("#templates [template-name='report-error'] > *").clone();
    noReport.find("[error-pos='main']").text(message.main);
    noReport.find("[error-pos='sub']").text(message.sub);

    main.append(noReport);
};

let formatNumberValue = (value, type) => {
    switch(type) {
        case "number":
            return (value ? value : Number(0)).round(userConfig.numberDigitRound);
        case "currency":
            // round to two digits
            return (value ? value : Number(0)).format();
        default:
            return value;
    }
};

let formatValue = (value, type) => {
    switch(type) {
        case "text":
            return value;
        case "number":
        case "currency":
            if (!Number.isNaN(value))
                return formatNumberValue(value, type);
            else
                return NaN;
        default:
            return value;
    }
};

let fillLayout = (data, userData = {}) => {
    main.html("");
    main.append("<div class='report-content'>");

    let content = main.children(".report-content");
    let rows = data;

    for (var i = 0; i < rows.length; i++) {
        let newRow = $("<div class='report-row'>");
        let tiles = rows[i];

        for (var j = 0; j < tiles.length; j++) {
            let newTile = $("<div class='report-tile' tile-id='" + tiles[j].id + "'>");
            newTile.on("save", function(e) {
                console.log("test");
            });

            let tileUserData = (tiles[j].id in userData ? userData[tiles[j].id] : null);

            let name = tileUserData ? tileUserData.name : tiles[j].name;
            let body = tiles[j].layout;

            let bodyType = body.type;
            let bodyID = body.id;
            let bodyValue = body.value;

            let tileHead = $("<div class='tile-head'>");
            let tileBody = $("<div class='tile-body'>");

            tileHead.css("background", tileUserData ? tileUserData.color : "#fff");

            let tileHeadName = $("<h4>" + name  + "</h4>");

            let tileToolbar = $("<ul class='report-tile-tools'>");
            let editTool = $("<li class='report-tile-tool' tool-id='edit'>");
            editTool.on("click", function() {
                let tileMenu = $("#tile-edit-slide");

                let tile = $(this).closest("div.report-tile");
                let tileID = tile.attr("tile-id");
                let lastID = tileMenu.attr("tile-id");

                tileMenu.attr("tile-id", tileID);
                tileMenu.find("input[name='tile-name']").val(tile.find(".tile-head h4").text());

                let color = tile.find(".tile-head").css("background-color");
                tileColorPicker.setColor(color);

                if (lastID === tileID)
                    toggleSlideMenu("#tile-edit-slide");
                else
                    toggleSlideMenu("#tile-edit-slide", "show");
            });

            tileToolbar.append(editTool);

            tileHead.append(tileHeadName);
            tileHead.append(tileToolbar);

            switch(bodyType) {
                case "table":
                    // The bodyValue represents a table object so create a table from it
                    let table = $("<table>");

                    let appendData = (type, data, childType = "td") => {
                        let tableType = "t" + type;
                        let obj = $("<" + tableType + ">");

                        for (var x = 0; x < data.length; x++) {
                            let newRow = $("<tr>");
                            let fields = data[x];

                            for (var y = 0; y < fields.length; y++)
                                newRow.append("<" + childType
                                    + (fields[y].type ? " report-field-type='" + fields[y].type + "'" : "")
                                    + (fields[y].id ? " report-id='" + fields[y].id + "'" : "") + ">"
                                    + (fields[y].value ? fields[y].value : "")
                                    + "</" + childType + ">");
                        
                            obj.append(newRow);
                        }

                        table.append(obj);
                    };

                    let tableHeadData = bodyValue.head;
                    let tableBodyData = bodyValue.body;
                    let tableFootData = bodyValue.foot;

                    appendData("head", tableHeadData, "th");
                    appendData("body", tableBodyData);
                    appendData("foot", tableFootData);

                    tileBody.append(table);

                    break;
                case "number":
                    // The bodyData represents a number so create a number from it
                    tileBody.append("<a report-field-type='number'"
                        + (bodyID ? " report-id='" + bodyID + "'" : "") + ">"
                        + (bodyValue ? bodyValue : "")
                        + "</a>");
                    break;
                default:
                    break;
            }

            newTile.append(tileHead);
            newTile.append(tileBody);

            newRow.append(newTile);
        }
        
        content.append(newRow);
    }    
};

let fillData = (data, timestamp = 0) => {
    let keys = Object.keys(data);

    for (var i = 0; i < keys.length; i++) {
        let key = keys[i];
        let value = data[key];

        let obj = main.find("[report-id='" + key + "']");
        
        if (obj.length === 0)
            console.warn("There is no field with id '" + key + "' in this report");

        if (obj.is("[timestamp]")) {
            let lastTime = obj.attr("timestamp");

            if (timestamp > lastTime) {
                obj.text(formatValue(value, obj.attr("report-field-type")));
                obj.attr("timestamp", timestamp);
            }
        } else {
            obj.text(formatValue(value, obj.attr("report-field-type")));
            obj.attr("timestamp", timestamp);
        }
    }
};

let connectToReport = (host, report, userData = {}) => {
    if (wsClient)
        wsClient.terminate();
    
    wsClient = new Worker("/reportWorker");
    wsClient.onmessage = (msg) => {
        let data = JSON.parse(msg.data);

        switch(data.topic) {
            case "ping_result":
                pingStatus.text(data.duration);

                if (data.duration <= 20)
                    pingStatus.attr("speed-index", "high");
                else if (data.duration <= 50)
                    pingStatus.attr("speed-index", "medium");
                else if (data.duration <= 100)
                    pingStatus.attr("speed-index", "low");
                else
                    pingStatus.attr("speed-index", "no");
                break;
            case "layout":
                fillLayout(data.data, userData);
                break;
            case "data":
                fillData(data.data, data.timestamp);
                break;
            case "error":
                connectionError({
                    main: "Error in WebSocket connection",
                    sub: "Can't connect to the report '" + report + "' on host '" + host +  "'"
                });

                pingStatus.text("-");

                lifetimeStatus.removeClass("short-time");
                lifetimeMinuteStatus.text("00");
                lifetimeSecondStatus.text("00");
            default:
                break;
        }
    };
    wsClient.postMessage(JSON.stringify({
        host: host,
        report: report
    }));
};

let connect = () => {
    let configOptions = {
        url: "/userConfig",
        type: "GET",
        dataType: "json",
        headers: SESSION_HEADER()
    };

    $.ajax(configOptions)
    .done((result) => {
        userConfig.reportHost = result.reportHost;
        userConfig.lastReport = result.lastReport;
        userConfig.requestErrorLivetime = result.requestErrorLivetime;
        userConfig.numberDigitRound = result.numberDigitRound;

        tryConnect();
    })
    .fail((error) => {
        if (error.status === 403)
            connectionError({
                main: "Authorization error",
                sub: "User was logged out"
            });

        toggleSlideMenu("#user-slide");
        showUserMenu("#user-login");

        document.cookie = "sessionID=" + global.SESSION_ID + ";path=/;domain=." + location.hostname + ";expires=Thu, 01 Jan 1970 00:00:00 GMT";
    });
}

$(".slide-menu-toggle").on("click", function(e) {
    e.stopPropagation();
    e.preventDefault();

    let target = $(this).attr("target");

    toggleSlideMenu(target);
});

$(".slide-menu-close").on("click", function(e) {
    e.stopPropagation();
    e.preventDefault();

    let target = "#" + $(this).closest(".slide-menu").prop("id");

    toggleSlideMenu(target);
});

$("#report").on("click", (e) => {
    e.stopPropagation();
    e.preventDefault();

    if (!$(e.target).is(".report-tile-tool"))
        toggleSlideMenu(null);
});

$("#user-login").on("submit", function(e) {
    e.preventDefault();

    let data = {
        username: $(this).find("[name='username']").val(),
        password: $(this).find("[name='password']").val()
    };

    let options = {
        url: "/login",
        type: "POST",
        dataType: "json",
        data: data
    };

    $.ajax(options)
    .done((result) => {
        global.SESSION_ID = result;
        document.cookie = "sessionID=" + global.SESSION_ID + ";path=/;domain=." + location.hostname;

        connect();
        toggleSlideMenu("#user-slide");
        showUserMenu("#user-logout");
    })
    .fail((error) => {
        connectionError({
            main: "Authorization error",
            sub: "User could not be logged in"
        });

        onRequestError(error, options);
    });

    return false;
});

$("#user-logout").on("submit", function(e) {
    e.preventDefault();

    let options = {
        url: "/logout",
        type: "GET",
        dataType: "json",
        headers: SESSION_HEADER()
    };

    $.ajax(options)
    .done(() => {
        showUserMenu("#user-login");

        lifetimeStatus.removeClass("short-time");
        lifetimeMinuteStatus.text("00");
        lifetimeSecondStatus.text("00");
    })
    .fail((error) => {
        connectionError({
            main: "Authorization error",
            sub: "User could not be logged out"
        });

        onRequestError(error, options);
    })
    .always(() => {
        document.cookie = "sessionID=" + global.SESSION_ID + ";path=/;domain=." + location.hostname + ";expires=Thu, 01 Jan 1970 00:00:00 GMT";
    });

    return false;
});

$("#user-register").on("submit", function(e) {
    e.preventDefault();

    let data = {
        username = $(this).find("[name='username']").val(),
        password = $(this).find("[name='password']").val()
    };

    let options = {
        url: "/register",
        type: "POST",
        dataType: "json",
        data: data
    };

    $.ajax(options)
    .done((result) => {
        let userLogin = $("#user-login");
        userLogin.find("[name='username']").val(result.username);
        userLogin.find("[name='password']").val(result.password);

        showUserMenu("#user-login");
    })
    .fail((error) => {
        connectionError({
            main: "Registration Error",
            sub: "Could not register user on server"
        });

        onRequestError(error, options);
    });

    return false;
});

tileColorPicker = new Picker({
    parent: document.querySelector("#tile-edit-slide-content-color-picker"),
    popup: false,
    editor: false,
    cancelButton: false,
    onChange: changeColorOnTile
});

if (document.cookie) {
    let sessID = document.cookie.match(new RegExp("sessionID=(.*?)(?:;|$)"));
    global.SESSION_ID = sessID ? sessID.pop() : null;

    if (global.SESSION_ID) {
        connect();

        showUserMenu("#user-logout");
    } else
        toggleSlideMenu("#user-slide");
} else
    toggleSlideMenu("#user-slide");

$("#tile-edit-slide").on("onhide", function(e) {
    $(this).removeAttr("tile-id");
});

$(".report-tile").on("save", function(e) {
    // TODO save the tile for the user
});