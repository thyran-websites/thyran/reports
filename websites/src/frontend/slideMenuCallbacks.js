let onRequestError = (error, options) => {
    requestError({
        method: options.type,
        url: options.url,
        code: error.status,
        text: error.statusText
    });
};

let refreshReports = (callback) => {
    let options = {
        url: "/getReports",
        type: "GET",
        dataType: "json",
        headers: SESSION_HEADER()
    };

    let onDone = (result) => {
        let anchor = $("#report-slide-content");
        anchor.html("");

        if (result.length > 0)
            result.forEach(report => {
                let reportOption = $("<div class='report-option' report-id='" + report.id + "'>");
                let reportOptionHeader = $("<div class='report-option-header'>");
                let reportName = $("<a class='report-option-name'>" + report.name + "</a>");
                let collapse = $("<span class='report-option-collapse'>");
                let reportOptionBody = $("<div class='report-option-body'>");
                let reportOptionBodyContent = $("<div class='report-option-content'>");

                reportName.on("click", function(e) {
                    let newReport = $(this).closest(".report-option").attr("report-id");
                    
                    if (userConfig.lastReport != newReport) {
                        let changeReportOptions = {
                            url: "/changeReport",
                            type: "POST",
                            dataType: "json",
                            data: {
                                report: newReport
                            },
                            headers: SESSION_HEADER()
                        };

                        $.ajax(changeReportOptions)
                        .done(() => {
                            userConfig.lastReport = newReport;

                            tryConnect();
                            toggleSlideMenu("#report-slide");
                        })
                        .fail((error) => {
                            onRequestError(error, changeReportOptions);
                        });
                    }
                });

                collapse.on("click", function(e) {
                    $(this).closest(".report-option").toggleClass("collapsed");
                });

                reportOptionBodyContent.text(report.description);

                reportOptionBody.append(reportOptionBodyContent);
                reportOptionHeader.append(reportName);
                reportOptionHeader.append(collapse);
                reportOption.append(reportOptionHeader);
                reportOption.append(reportOptionBody);
                anchor.append(reportOption);
            });
        else
            anchor.append("<span class='slide-warning'>No reports were found</span>");

        if (callback && typeof callback === typeof Function)
            callback();
    };

    $.ajax(options)
    .done(onDone)
    .fail((error) => { onRequestError(error, options); });
};

function getSlideMenuCallbacks() {
    let hideReportSlide = (callback) => {
        callback();
    };

    return {
        "report-slide": {
            show: refreshReports,
            hide: hideReportSlide
        }
    };
}