let initial = {
    layout: false,
    data: false
};

let latestData = {
    data: null,
    timestamp: 0
};

let start = (config, host, report) => {
    let wsClient = new WebSocket(location.protocol.replace("http", "ws") + "//" + host + "/" + report, "report");
    let pingServer = () => {
        // execute a ping to the server
        if (wsClient.readyState === WebSocket.OPEN)
        wsClient.send(JSON.stringify({
            topic: "ping",
            timestamp: Date.now()
        }));
    };

    wsClient.onopen = () => {
        let oldPingInterval = config.pingInterval;
        let pingCheckIntervalID;
        let checkPing = () => {
            // check if value changed
            let newPingInterval = config.pingInterval;
            if (newPingInterval != oldPingInterval) {
                // set new interval
                clearInterval(pingCheckIntervalID);
                oldPingInterval = newPingInterval;
                pingCheckIntervalID = setInterval(checkPing, !oldPingInterval ? pingIntervalFallback : (oldPingInterval < 1000 ? 1000 : oldPingInterval));
            }

            pingServer();
        };
        pingCheckIntervalID = setInterval(checkPing, !oldPingInterval ? pingIntervalFallback : (oldPingInterval < 1000 ? 1000 : oldPingInterval));
        pingServer();
    };

    wsClient.onmessage = (data) => {
        let msg = JSON.parse(data.data);

        if (msg.topic === "pong") {
            self.postMessage(JSON.stringify({
                topic: "ping_result",
                duration: msg.duration
            }));
            
            return;
        }

        switch(msg.topic) {
            case "initial":
                if (msg.subtopic === "layout") {
                    let reportData = msg.data;

                    self.postMessage(JSON.stringify({
                        topic: "layout",
                        data: reportData
                    }));

                    if (initial.data)
                        self.postMessage(JSON.stringify({
                            topic: "data",
                            data: latestData.data,
                            timestamp: latestData.timestamp
                        }));

                    initial.layout = true;
                } else if (msg.subtopic === "data") {
                    latestData = {
                        data: msg.data,
                        timestamp: msg.timestamp
                    };

                    if (initial.layout)
                        self.postMessage(JSON.stringify({
                            topic: "data",
                            data: latestData.data,
                            timestamp: latestData.timestamp
                        }));
                
                    initial.data = true;
                }

                if (initial.layout && initial.data && wsClient.readyState === WebSocket.OPEN)
                    wsClient.send(JSON.stringify({
                        topic: "enable",
                        data: true
                    }));
                break;
            case "update":
                latestData = {
                    data: msg.data,
                    timestamp: msg.timestamp
                };

                if (initial.layout)
                    self.postMessage(JSON.stringify({
                        topic: "data",
                        data: latestData.data,
                        timestamp: latestData.timestamp
                    }));
                break;
            default:
                console.warn("No handler for topic '" + msg.topic + "'");
                break;
        }
    };

    wsClient.onerror = (error) => {
        console.log(error);

        self.postMessage(JSON.stringify({
            topic: "error"
        }));
    };
}

self.addEventListener("message", (msg) => {
    let data = JSON.parse(msg.data);

    let request = new XMLHttpRequest();
    request.open("GET", "/reportWorkerConfig", true);
    request.onload = () => { start(JSON.parse(request.response), data.host, data.report); };
    request.send();
});