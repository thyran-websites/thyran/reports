let pingIntervalID = null;

let start = (config, sessionID) => {
    let socket = new WebSocket(config.protocol.replace("http", "ws") + "://" + config.hostname + "/" + sessionID, "user");
    socket.onopen = () => {
        if (socket.readyState === WebSocket.OPEN) {
            socket.send(JSON.stringify({
                topic: "refresh_user"
            }));

            socket.send(JSON.stringify({
                topic: "user_maxLifetime"
            }));
        }
    };
    socket.onmessage = (msg) => {
        let data = JSON.parse(msg.data);

        switch(data.topic) {
            case "user_status":
                let isLoggedIn = data.loggedIn;

                if (!isLoggedIn) {
                    self.postMessage(JSON.stringify({
                        type: "login",
                        session: sessionID,
                        status: false
                    }));
                }
                break;
            case "user_lifetime":
                self.postMessage(JSON.stringify({
                    type: data.topic,
                    lifetime: data.lifetime
                }));
                break;
            default:
                break;
        }
    };
    socket.onclose = (e) => {
        self.postMessage(JSON.stringify({
            type: "connection",
            status: false
        }));

        pingIntervalID = setInterval(() => {
            let ping = new XMLHttpRequest();
            ping.open("GET", "/liveCheck");
            ping.onload = () => {
                self.postMessage(JSON.stringify({
                    type: "refresh",
                    status: true
                }));

                clearInterval(pingIntervalID);
            };
            ping.send();
        }, config.liveCheckInterval);
    };

    setInterval(() => {
        if (socket.readyState === WebSocket.OPEN)
            socket.send(JSON.stringify({
                topic: "user_lifetime"
            }));
    }, 1000);
};

self.addEventListener("message", (msg) => {
    let data = JSON.parse(msg.data);

    if (!data.start) return;

    let request = new XMLHttpRequest();
    request.open("GET", "/userWorkerConfig", true);
    request.onload = function() { start(JSON.parse(request.response), data.sessionID); };
    request.send();
});