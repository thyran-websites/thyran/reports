<?php
$host = $_SERVER["HTTP_REFERER"];

$username = $_POST["username"];
$password = $_POST["password"];

$password = password_hash($password, PHP_PASSWORD_DEFAULT);

$stream = array(
    "http" => array(
        "method" => "POST",
        "content" => json_encode(array(
            "username" => $username,
            "password" => $password
        ))
    )
);

$loginResult = file_get_contents($host . "login", false, stream_context_create($stream));

exit(json_decode($loginResult));
?>