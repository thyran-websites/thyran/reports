global.__basedir = __dirname;

const http = require("http");
const https = require("https");
const express = require("express");
const fs = require("fs");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const reports = require("./globals").reports;
const endpoints = require("./globals").endpoints;
const users = require("./globals").users;
const groups = require("./globals").groups;

const vhost = require("vhost");
const exitHook = require("exit-hook");
const reportServer = require("./servers/reportServer");
const dataServer = require("./servers/dataServer");

const Report = require("./classes/Report");
const Endpoint = require("./classes/Endpoint");
const User = require("./classes/User");
const Group = require("./classes/Group");

var useSSL = {
    report: false,
    data: false
};

const configFilePath = "./config.json";
var config = JSON.parse(fs.readFileSync(configFilePath).toString());

const host = config.host;
const hostname = config.hostname;

const reportPort = config.ports.reportServer;
const dataPort = config.ports.dataServer;

const sslDataPort = config.ssl.ports.dataServer;
const sslReportPort = config.ssl.ports.reportServer;

const sqlArgs = {
    host: config.sql.host,
    user: config.sql.user,
    password: config.sql.pass,
    database: config.sql.database
};

const userRefreshInterval = config.userRefreshInterval;

global.hashSaltRounds = config.hashSaltRounds;

exitHook(() => {
    // TODO save reports and endpoints to database
});

let initialize = () => {
    let connectIntervalID = null;
    let connectDatabase = () => {
        let connection = mysql.createConnection(sqlArgs);
        connection.connect((err) => {
            if (err) {
                console.warn("Database could not be connected");

                if (!connectIntervalID)
                    connectIntervalID = setInterval(connectDatabase, 5000);

                return;
            } else if (connectIntervalID) {
                console.log("Database connected");

                clearInterval(connectIntervalID);
            }

            connection.query(fs.readFileSync(__basedir + "/sql/get_endpoints.sql").toString(), (err, result) => {
                if (err) throw err;

                result.forEach(e => {
                    endpoints.set(e.id, new Endpoint(e.id));
                    endpoints.get(e.id).init(sqlArgs);
                });

                connection.query(fs.readFileSync(__basedir + "/sql/get_reports.sql").toString(), (err, result) => {
                    if (err) throw err;

                    result.forEach(r => {
                        reports.set(r.id, new Report(r.id));
                        Report.getFromID(r.id).init(sqlArgs);
                    });

                    connection.query(fs.readFileSync(__basedir + "/sql/get_groups.sql").toString(), (err, result) => {
                        if (err) throw err;

                        result.forEach(g => {
                            groups.set(g.id, new Group(g.id));
                            Group.getFromID(g.id).init(sqlArgs);
                        });

                        connection.end((err) => { if (err) throw err; });
                    });
                });
            });
        });
    };

    connectDatabase();

    // set up the user refresh
    setInterval(() => {
        Object.values(users.all).forEach(user => {
            user.checkLifetime();
        });
    }, userRefreshInterval);
};

initialize();

var app = express();

var reportHTTPServer = http.createServer(app).listen(reportPort, host, () => {
    console.log("report server listening on port " + reportPort);
});
var dataHTTPServer = http.createServer().listen(dataPort, host, () => {
    console.log("data server listening on port " + dataPort);
});

let frontend = require("./app_frontend");
let backend = require("./app_backend");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
    if (useSSL.report && req.protocol === "http")
        return res.redirect("https://" + req.hostname + ":" + sslReportPort + req.url);

    next();
});

app.use((req, res, next) => {
    let user = User.getFromRequest(req);
    if (user)
        user.extendLifetime();

    next();
});

app.use(vhost(hostname, frontend));
app.use(vhost("backend." + hostname, backend));

app.use("/res", express.static(__basedir + "/websites/res"));

app.get("/jquery.js", (req, res) => { res.sendFile(__basedir + "/websites/dist/jquery.js"); });
app.get("/reportWorker", (req, res) => { res.sendFile(__basedir + "/websites/dist/reportWorker.min.js"); });
app.get("/userWorker", (req, res) => { res.sendFile(__basedir + "/websites/dist/userWorker.min.js"); });
app.get("/color-picker.js", (req, res) => { res.sendFile(__basedir + "/websites/dist/vanilla-picker.min.js"); });
app.get("/liveCheck", (req, res) => { res.sendStatus(200); });

app.post("/login", (req, res) => {
    let data = req.body;

    let newUser = new User(req.headers.host, sqlArgs);
    newUser.login(data.username, data.password, (sessionID) => {
        if (sessionID) {
            users.set(sessionID, newUser);
            res.send(JSON.stringify(sessionID));
        } else
            res.sendStatus(404);
    });
});

app.get("/logout", (req, res) => {
    let user = User.getFromRequest(req);
    User.removeUser(user);

    res.send(JSON.stringify(true));
});

app.post("/register", (req, res) => {
    let data = req.body;
    
    if ((!("username" in data) || !data.username) || (!("password" in data) || !data.password)) {
        res.sendStatus(401);
        return;
    }

    User.registerUser(data.username, data.password, sqlArgs, (inserted) => {
        if (inserted)
            res.send(JSON.stringify({
                username: data.username,
                password: data.password
            }));
        else
            res.sendStatus(500);
    });
});

app.get("/userWorkerConfig", (req, res) => {
    res.send(JSON.stringify({
        protocol: req.protocol,
        hostname: req.headers.host,
        liveCheckInterval: 10000
    }));
});

app.get("/reportWorkerConfig", (req, res) => {
    res.send(JSON.stringify({
        pingInterval: config.reportSocketPingInterval
    }));
});

let isReportHTTPOriginAllowed = (origin) => {
    return true;
};

let isReportHTTPSOriginAllowed = (origin) => {
    return true;
};

let isDataHTTPOriginAllowed = (origin) => {
    return true;
};

let isDataHTTPSOriginAllowed = (origin) => {
    return true;
};

reportServer(reportHTTPServer, isReportHTTPOriginAllowed, "Report Server");
dataServer(dataHTTPServer, isDataHTTPOriginAllowed, "Data Server");

if (config.ssl.active) {
    if (fs.existsSync(config.ssl.credentials.reportServer.pKey) && fs.existsSync(config.ssl.credentials.reportServer.cert)) {
        let credentials = {
            key: fs.readFileSync(config.ssl.credentials.reportServer.pKey),
            cert: fs.readFileSync(config.ssl.credentials.reportServer.cert)
        };

        let reportHTTPSServer = https.createServer(credentials, app).listen(sslReportPort, () => {
            console.log("Secured Report Server listening on port " + sslReportPort);

            useSSL.report = true;
        });

        reportServer(reportHTTPSServer, isReportHTTPSOriginAllowed, "Secured Report Server");
    }

    if (fs.existsSync(config.ssl.credentials.dataServer.pKey) && fs.existsSync(config.ssl.credentials.dataServer.cert)) {
        let credentials = {
            key: fs.readFileSync(config.ssl.credentials.dataServer.pKey),
            cert: fs.readFileSync(config.ssl.credentials.dataServer.cert)
        };

        let dataHTTPSServer = https.createServer(credentials).listen(sslDataPort, () => {
            console.log("Secured Data Server listening on port " + sslDataPort);

            useSSL.data = true;
        });

        dataServer(dataHTTPSServer, isDataHTTPSOriginAllowed, "Secured Data Server");
    }
}