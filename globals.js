let reports = {};
let users = {};
let endpoints = {};
let groups = {};

module.exports = {
    reports: (() => {
        let set = (id, report) => { reports[id] = report; };
        let get = (id) => { return reports[id]; };
        let rem = (id) => { delete reports[id]; };

        return {
            set: set,
            get: get,
            rem: rem,
            all: reports
        }
    })(),
    users: (() => {
        let set = (id, user) => { users[id] = user; };
        let get = (id) => { return users[id]; };
        let rem = (id) => { delete users[id]; };

        return {
            set: set,
            get: get,
            rem: rem,
            all: users
        }
    })(),
    endpoints: (() => {
        let set = (id, endpoint) => { endpoints[id] = endpoint; };
        let get = (id) => { return endpoints[id]; };
        let rem = (id) => { delete endpoints[id]; };

        return {
            set: set,
            get: get,
            rem: rem,
            all: endpoints
        }
    })(),
    groups: (() => {
        let set = (id, group) => { groups[id] = group; };
        let get = (id) => { return groups[id]; };
        let rem = (id) => { delete groups[id]; };

        return {
            set: set,
            get: get,
            rem: rem,
            all: groups
        }
    })()
};