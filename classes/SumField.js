const CalculateField = require("./CalculateField");
const ReportValueType = require("./ReportValueType");
const typeGetter = require("./FieldFunctions").getType;

class SumField extends CalculateField {
    constructor(id) {
        super(id);

        this.fields = [];
    }

    init(value, report) {
        this.setType(ReportValueType.NUMBER);

        let fields = report.getFields();
        value.fields.forEach(key => {
            if (fields[key])
                this.addField(fields[key]);
            else
                console.warn("The field key '" + key + "' for sum field with key '" + this.getID() + "' does not exist in report '" + report.getID() + "'");
        });
    }

    save() {
        return {
            type: typeGetter(this),
            fields: this.fields.map(f => f.getID())
        };
    }

    addField(field) {
        if (field.getType() === ReportValueType.NUMBER) {
            this.fields.push(field);
            this.addDependendField(field);
        }
    }

    doCalculate() {
        return this.fields.map(f => f.getValue()).reduce((a, b) => { return a + b; });
    }

    getFields() { return this.fields; }
}

module.exports = SumField;