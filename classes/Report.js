const DatabaseObject = require("./DatabaseObject");
const Connection = require("websocket").connection;
const CalculateField = require("./CalculateField");
const Endpoint = require("./Endpoint");
const InputField = require("./InputField");
const TranslationMap = require("./TranslationMap");

const fs = require("fs");
const reports = require("../globals").reports;

const getFieldFromType = require("./FieldFunctions").getField;

const ReportState = {
    STOPPED: 0,
    RUNNING: 1
};

class Report extends DatabaseObject {
    constructor(id) {
        super();

        this.id = id;
        this.databaseID = null;
        this.connections = {};
        this.endpoints = [];

        this.layout = {};
        this.fields = {};
        this.translations = {};

        this.name = null;
        this.description = null;

        this.state = ReportState.STOPPED;
    }

    onInit(self, connection, resolve, reject) {
        connection.query(fs.readFileSync(__basedir + "/sql/get_report.sql").toString(), [self.getID()], (err, result) => {
            if (err) reject(err);

            if (result.length > 0) {
                let report = result[0];

                self.setDatabaseID(report.id);
                self.setName(report.name);
                self.setDescription(report.description);

                if (report.active)
                    self.start();

                // init layout
                connection.query(fs.readFileSync(__basedir + "/sql/get_reportLayout.sql").toString(), [report.id], (err, result) => {
                    if (err) reject(err);

                    let newLayout = [];
                    result.forEach(row => {
                        if (row.row_index === newLayout.length)
                            newLayout.push([]);

                        if (row.tile_index === newLayout[row.row_index].length)
                            newLayout[row.row_index].push([]);

                        let newTile = {
                            id: row.tile_id,
                            name: row.name,
                            layout: JSON.parse(row.layout)
                        };

                        newLayout[row.row_index][row.tile_index] = newTile;
                    });

                    self.changeLayout(newLayout);
                });

                // init fields
                connection.query(fs.readFileSync(__basedir + "/sql/get_reportFields.sql").toString(), [report.id], (err, result) => {
                    if (err) reject(err);

                    result.forEach(row => self.addField(row.field_id, getFieldFromType(row.field_id, row.type)));
                    
                    Object.values(self.getFields()).forEach((f, index) => {
                        f.init(JSON.parse(result[index].config), self);
                        f.setValue(JSON.parse(result[index].value));
                    });
                });

                // init translations and endpoints
                connection.query(fs.readFileSync(__basedir + "/sql/get_reportTranslations.sql").toString(), [report.id], (err, result) => {
                    if (err) reject(err);

                    let lastEndpointID = null;
                    let map = {};

                    let addMap = (endpointID, map, self) => {
                        let endpoint = Endpoint.getFromID(endpointID);
                        self.addEndpoint(endpoint);
                        self.addTranslationMap(endpoint.getID(), new TranslationMap(endpoint, map, self));

                        map = {};
                    }

                    result.forEach(row => {
                        map[row.endpoint_input_id] = row.report_field_id;

                        if (lastEndpointID != row.endpoint_id) {
                            if (lastEndpointID)
                                addMap(row.system_id, map, self);

                            lastEndpointID = row.system_id;
                        }
                    });

                    if (lastEndpointID)
                        addMap(lastEndpointID, map, self);
                });

                resolve();
            } else
                reject("No rows were exported");
        });
    }

    onSave(self, connection, resolve, reject) {
        // TODO save report in database
        resolve();
    }

    start() { this.state = ReportState.RUNNING; }
    stop() { this.state = ReportState.STOPPED; }

    setDatabaseID(databaseID) { this.databaseID = databaseID; }
    setName(name) { this.name = name; }
    setDescription(description) { this.description = description; }

    calculate(data) {
        Object.keys(data).forEach(key => {
            if (this.fields.hasOwnProperty(key) && this.fields[key] instanceof InputField)
                this.fields[key].input(data[key]);
        });

        Object.keys(this.fields).forEach(key => {
            let field = this.fields[key];

            if (field instanceof CalculateField) {
                field.calculate();
            }
        });
    }

    update(data, endpoint) {
        let translated = data;

        if (this.translations[endpoint.getID()])
            translated = this.translations[endpoint.getID()].translate(data);
        else
            console.warn("No translation map for endpoint " + endpoint.getID() + "' in report '" + this.name + "'");

        this.calculate(translated);

        let msg = {
            topic: "update",
            data: this.getData(),
            timestamp: Date.now()
        };

        Object.keys(this.connections).forEach(k => this.connections[k].send(JSON.stringify(msg)));
    }

    changeLayout(layout) {
        this.layout = layout;
    }

    /**
     * Adds a connection to this report
     * @param {Connection} connection the connection to add
     */
    addConnection(connection) {
        this.connections[connection.guid] = connection;
    }

    /**
     * Removes a connection from a report
     * @param {Connection} connection the connection to remove
     */
    removeConnection(connection) {
        delete this.connections[connection.guid];
    }

    /**
     * Adds an enpoint to this report
     * @param {Endpoint} endpoint the endpoint to add
     */
    addEndpoint(endpoint) {
        if (!this.endpoints.map(e => e.getID().toLowerCase()).includes(endpoint.getID().toLowerCase()))
            this.endpoints.push(endpoint);
        
        endpoint.addReport(this);
    }

    addField(key, field) {
        if (field)
            this.fields[key] = field;
        else
            console.warn("Undefined type for field with id '" + key + "' in report '" + this.getID() + "'");
    }

    addTranslationMap(key, translationMap) {
        this.translations[key] = translationMap;
    }

    /**
     * Sends initial data from this report to a connection
     * @param {Connection} connection the connection to send to
     */
    sendInitialData(connection) {
        connection.send(JSON.stringify({
            topic: "initial",
            subtopic: "layout",
            data: this.getLayout(),
            timestamp: Date.now()
        }));
    
        connection.send(JSON.stringify({
            topic: "initial",
            subtopic: "data",
            data: this.getData(),
            timestamp: Date.now()
        }));
    }

    getData() {
        return Object.keys(this.fields)
            .reduce((obj, key) => {
                obj[key] = this.fields[key].getValue();
                return obj;
            }, {});
    }

    getFields() { return this.fields; }
    getLayout() { return this.layout; }
    getID() { return this.id; }
    getDatabaseID() { return this.databaseID; }
    getName() { return this.name; }
    getDescription() { return this.description; }
    getConnections() { return this.connections; }
    getEndpoints() { return this.endpoints; }
    isRunning() { return this.state === ReportState.RUNNING; }

    static getFromID(id) {
        return id ? reports.get(id) : null;
    }

    static getAll() {
        return reports.all;
    }
}

module.exports = Report;