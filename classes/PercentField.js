const CalculateField = require("./CalculateField");
const ReportValueType = require("./ReportValueType");
const typeGetter = require("./FieldFunctions").getType;

class PercentField extends CalculateField {
    constructor(id) {
        super(id);

        this.percentBase = null;
        this.percentValue = null;
    }

    init(value, report) {
        this.setType(ReportValueType.NUMBER);

        let fields = report.getFields();
        if (fields[value.percentBase])
            this.setPercentBase(fields[value.percentBase]);
        else
            console.warn("The field key '" + value.percentBase + "' for percent field with key '" + this.getID() + "' does not exist in report '" + report.getID() + "'");

        if (fields[value.percentValue])
            this.setPercentValue(fields[value.percentValue]);
        else
            console.warn("The field key '" + value.percentValue + "' for percent field with key '" + this.getID() + "' does not exist in report '" + report.getID() + "'");
    }

    save() {
        return {
            type: typeGetter(this),
            percentBase: this.percentBase.getID(),
            percentValue: this.percentValue.getID()
        };
    }

    setPercentBase(field) {
        if (field.getType() === ReportValueType.NUMBER) {
            this.percentBase = field;
            this.addDependendField(field);
        }
    }

    setPercentValue(field) {
        if (field.getType() === ReportValueType.NUMBER) {
            this.percentValue = field;
            this.addDependendField(field);
        }
    }

    doCalculate() {
        return (this.percentValue.getValue() / this.percentBase.getValue()) * 100;
    }

    getPercentBase() { return this.percentBase; }
    getPercentValue() { return this.percentValue; }
}

module.exports = PercentField;