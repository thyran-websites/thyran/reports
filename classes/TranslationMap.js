class TranslationMap {
    constructor(endpoint, map, report) {
        this.endpoint = endpoint;
        this.map = map;
        this.report = report;
    }

    translate(data) {
        return Object.keys(data)
            .filter(key => {
                let valid = Object.keys(this.map).includes(key);

                if (!valid)
                    console.warn("Input key '" + key + "' is not in translation map for endpoint '" + this.endpoint.getID() + "' for report '" + this.report.getID() + "'");

                return valid;
            })
            .reduce((obj, key) => {
                obj[this.map[key]] = data[key];
                return obj;
            }, {});
    }

    save() {
        return this.map;
    }
}

module.exports = TranslationMap;