const ReportField = require("./ReportField");
const ReportValueType = require("./ReportValueType");

class CalculateField extends ReportField {
    constructor(id) {
        super(id);

        this.dependedFields = [];
        this.updateNeeded = true;
    }

    addDependendField(field) {
        this.dependedFields.push(field);
        field.addSuperField(this);
    }

    doCalculate()  {
        throw new Error("You need to implement the method doCalculate()");
    }

    calculate() {
        this.dependedFields.forEach(f => {
            if (f instanceof CalculateField && !f.isOutDated())
                f.calculate();
        });

        if (this.needsUpdate) {
            this.value = this.doCalculate();

            if (!this.value)
                switch(this.getType()) {
                    case ReportValueType.TEXT:
                        this.value = "";
                        break;
                    case ReportValueType.NUMBER:
                        this.value = 0;
                        break;
                    case ReportValueType.BOOLEAN:
                        this.value = false;
                        break;
                    default:
                        this.value = "";
                        break;
                }

            this.triggerUpdateNeed();
            this.updateNeeded = false;
        }
    }
}

module.exports = CalculateField;