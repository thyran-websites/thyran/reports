const ReportValueType = require("./ReportValueType");

class ReportField {
    /**
     * Creates a report field
     * @param {*} id the id for this field
     */
    constructor(id) {
        this.id = id;
        this.type = null;

        this.superFields = [];

        this.updateNeeded = false;
        this.value = null;
    }

    init(value, report) {
        throw new Error("You need to implement the method init()");
    }

    save() {
        throw new Error("You need to implement the method save()");
    }

    setValue(value) {
        this.value = value;
    }

    setType(type) {
        this.type = type;
    }

    needsUpdate() {
        this.updateNeeded = true;
    }

    addSuperField(field) {
        this.superFields.push(field);
    }

    triggerUpdateNeed() {
        this.superFields.forEach(f => {
            f.needsUpdate();
        });
    }

    /**
     * returns the id of this field
     * @returns {any} id
     */
    getID() { return this.id; }

    /**
     * @returns {any} value the value of this field
     */
    getValue() { return this.value; }

    /**
     * @return {ReportValueType} type
     */
    getType() { return this.type; }
    isOutDated() { return this.updateNeeded; }
}

module.exports = ReportField;