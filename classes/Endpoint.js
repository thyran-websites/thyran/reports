const DatabaseObject = require("./DatabaseObject");
const Report = require("./Report");

const fs = require("fs");
const endpoints = require("../globals").endpoints;

const EndpointState = {
    STOPPED: 0,
    RUNNING: 1
};

class Endpoint extends DatabaseObject {
    constructor(id) {
        super();

        this.id = id;
        this.state = EndpointState.STOPPED;
        this.inputIDs = [];

        this.reports = [];
    }

    onInit(self, connection, resolve, reject) {
        connection.query(fs.readFileSync(__basedir + "/sql/get_endpoint.sql").toString(), [self.getID()], (err, result) => {
            if (err) reject(err);

            if (result.length > 0) {
                let endpoint = result[0];

                if (endpoint.active)
                    self.start();

                connection.query(fs.readFileSync(__basedir + "/sql/get_endpointInputs.sql").toString(), [endpoint.id], (err, result) => {
                    if (err) reject(err);

                    result.map(row => row.id).forEach(id => self.addInputID(id));
                    resolve();
                });
            } else
                reject("No rows were exported");
        });
    }

    onSave(self, connection, resolve, reject) {
        // TODO save endpoint in database
        resolve();
    }

    start() {
        this.state = EndpointState.RUNNING;
    }

    stop() {
        this.state = EndpointState.STOPPED;
    }

    addInputID(id) {
        this.inputIDs.push(id);
    }

    /**
     * Adds a report to this endpoint
     * @param {Report} report the report to add 
     */
    addReport(report) {
        if (!this.reports.map(r => r.getID().toLowerCase()).includes(report.getID().toLowerCase()))
            this.reports.push(report);
    }

    input(data) {
        this.reports.forEach(r => r.update(Object.keys(data)
            .filter(key => {
                let valid = this.inputIDs.includes(key);

                if (!valid)
                    console.warn("Key '" + key + "' is blocked in endpoint '" + this.getID() + "'");

                return valid;
            })
            .reduce((obj, key) => {
                obj[key] = data[key];
                return obj;
            }, {}),
            this
        ));
    }

    getID() { return this.id; }
    isRunning() { return this.state === EndpointState.RUNNING; }

    static getFromID(id) {
        return id ? endpoints.get(id) : null;
    }

    static getAll() {
        return endpoints.all;
    }
}

module.exports = Endpoint;