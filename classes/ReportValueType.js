const ReportValueType = {
    TEXT: 1,
    NUMBER: 2,
    BOOLEAN: 3
};

module.exports = ReportValueType;