const ReportFieldType = require("./ReportFieldType");

/**
 * gets a field from a reportFieldType
 * @param {*} id
 * @param {ReportFieldType} type 
 */
const getFieldFromType = (id, type) => {
    const InputField = require("./InputField");
    const SumField = require("./SumField");
    const PercentField = require("./PercentField");

    switch(type) {
        case ReportFieldType.INPUT:
            return new InputField(id);
        case ReportFieldType.SUM:
            return new SumField(id);
        case ReportFieldType.PERCENT:
            return new PercentField(id);
        default:
            return null;
    }
};

const getTypeFromField = (field) => {
    const InputField = require("./InputField");
    const SumField = require("./SumField");
    const PercentField = require("./PercentField");

    if (field instanceof InputField) return ReportFieldType.INPUT;
    if (field instanceof SumField) return ReportFieldType.SUM;
    if (field instanceof PercentField) return ReportFieldType.PERCENT;
}

module.exports = {
    getField: getFieldFromType,
    getType: getTypeFromField
};