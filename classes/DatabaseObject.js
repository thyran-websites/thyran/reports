const mysql = require("mysql");

class DatabaseObject {
    _execute(fn, args) {
        let connection = mysql.createConnection(args);

        new Promise((resolve, reject) => {
            connection.connect((err) => {
                if (err) {
                    reject("Database could not be connected");
                    return;
                }
            
                fn(this, connection, resolve, reject);
            });
        })
        .catch((err) => {
            console.warn(err);
        })
        .finally(() => {
            connection.end((err) => {
                if (err) throw err;
            });
        });
    }

    init(args) {
        this._execute(this.onInit, args);
    }

    save(args) {
        this._execute(this.onSave, args);
    }

    onInit(self, connection, resolve, reject) {
        throw new Error("You need to implement onInit(self, connection, resolve, reject)");
    }

    onSave(self, connection, resolve, reject) {
        throw new Error("You need to implement onSave(self, connection, resolve, reject)");
    }
}

module.exports = DatabaseObject;