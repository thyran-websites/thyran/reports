const ReportField = require("./ReportField");
const ReportValueType = require("./ReportValueType");
const typeGetter = require("./FieldFunctions").getType;

class InputField extends ReportField {
    /**
     * Creates a new 
     * @param {any} id 
     */
    constructor(id) {
        super(id);
    }

    init(value, report) {
        this.setType(value.inputType);
    }

    save() {
        return {
            type: typeGetter(this),
            inputType: this.getType()
        };
    }

    /**
     * Inputs a value into this field
     * @param {any} value 
     */
    input(value) {
        // field is an input field so input the data value
        switch(this.type) {
            case ReportValueType.TEXT:
                this.value = value;
                break;
            case ReportValueType.NUMBER:
                let number = Number.parseFloat(value);

                if (Number.isNaN(number))
                    this.value = 0;
                else
                    this.value = number;

                break;
            case ReportValueType.BOOLEAN:
                this.value = Boolean(data[key]).valueOf();
                break;
            default:
                this.value = null;
                break;
        }

        this.triggerUpdateNeed();
    }
}

module.exports = InputField;