const ReportFieldType = {
    INPUT: "INPUT",
    SUM: "SUM",
    PERCENT: "PERCENT"
};

module.exports = ReportFieldType;