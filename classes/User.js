const mysql = require("mysql");
const Guid = require("guid");
const bcrypt = require("bcrypt");
const fs = require("fs");
const users = require(__basedir + "/globals").users;

const Report = require("./Report");
const Group = require("./Group");

class User {
    constructor(hostname, sqlArgs) {
        this.sessionID = null;
        this.deathTime = 0;
        this.loggedIn = false;
        this.connection = null;
        this.hostname = hostname;

        this.sqlConnection = mysql.createConnection(sqlArgs);
        this.sqlConnection.connect((err) => { if (err) {
            console.warn("Database not connected");
            this.sqlConnection = null;
        }});

        this.userID = 0;
        this.maxAge = 0;
        this.lastReport = null;
        this.requestErrorLifetime = 0;
        this.numberDigits = 0;

        this.groups = [];
    }

    login(username, password, callback) {
        new Promise((resolve, reject) => {
            if (!this.sqlConnection) {
                console.warn("Could not login user " + username + ", database is not connected");
                reject();
                return;
            }

            this.sqlConnection.query(fs.readFileSync(__basedir + "/sql/get_user.sql").toString(), [username], (err, result) => {
                if (err || result.length === 0) reject();
                else {
                    let user = result[0];

                    bcrypt.compare(password, user.password, (err, same) => {
                        if (err || !same) reject();
                        else
                            this.sqlConnection.query(fs.readFileSync(__basedir + "/sql/get_user_groups.sql").toString(), [user.id], (err, result) => {
                                if (err) reject();

                                let groups = [];
                                result.forEach(row => {
                                    groups.push(Group.getFromID(row.group_id));
                                });

                                resolve({
                                    user: user,
                                    groups: groups
                                });
                            });
                    });
                }
            });
        })
        .then((obj) => {
            this.userID = obj.user.id;
            this.maxAge = obj.user.maxAge;
            this.lastReport = Report.getFromID(obj.user.report);
            this.requestErrorLifetime = obj.user.requestErrorLifetime;
            this.numberDigits = obj.user.numberDigits;

            this.groups = obj.groups;

            this.loggedIn = true;
            this.deathTime = Date.now() + this.maxAge;
            this.sessionID = Guid.create().value;

            callback(this.sessionID);
        })
        .catch(() => {
            callback(null);
        });
    }

    setConnection(connection) {
        this.connection = connection;
    }

    setLastReport(report) {
        this.lastReport = report;

        if (!this.sqlConnection) return;

        this.sqlConnection.query(fs.readFileSync(__basedir + "/sql/update_user_report.sql").toString(), [this.lastReport.getID(), this.userID], (err) => {
            if (err) throw err;
        });
    }

    extendLifetime() {
        this.deathTime = Date.now() + this.maxAge;
    }

    checkLifetime() {
        if (Date.now() > this.deathTime && this.loggedIn)
            User.removeUser(this);
    }

    getAuthorizedReports() {
        return this.groups.reduce((obj, value) => {
            value.getReports().forEach((report) => {
                if (!obj.map(r => r.getID()).includes(report.getID()))
                    obj.push(report);
            });
            return obj;
        }, []);
    }

    getConfig() {
        return {
            reportHost: this.hostname,
            lastReport: this.lastReport ? this.lastReport.getID() : null,
            requestErrorLifetime: this.requestErrorLifetime,
            numberDigitRound: this.numberDigits
        }
    }

    async getReportData(report) {
        let data = {};

        await new Promise((resolve, reject) => {
            this.sqlConnection.query(fs.readFileSync(__basedir + "/sql/get_userReportData.sql").toString(), [this.userID, report.getDatabaseID()], (err, result) => {
                if (err) reject(err);

                resolve(result.reduce((obj, value) => {
                    obj[value.tile_id] = {
                        color: value.color,
                        name: value.name
                    };

                    return obj;
                }, {}));
            });
        })
        .then((obj) => {
            data = obj;
        })
        .catch((err) => {
            console.log(err);

            data = {};
        });
        
        return data;
    }

    getLifetime() {
        return this.deathTime - Date.now();
    }

    isAuthorizedForReport(report) {
        return this.getAuthorizedReports().map(r => r.getID()).includes(report.getID());
    }

    kill() {
        this.connection.send(JSON.stringify({
            topic: "user_status",
            loggedIn: false
        }));

        this.sqlConnection.end();
    }

    getMaxAge() { return this.maxAge; }
    getSessionID() { return this.sessionID; }

    static getFromSessionID(sessionID) {
        return sessionID ? users.get(sessionID) : null;
    }

    static getFromRequest(request) {
        let session = request.headers["user-session-id"] === "null" ? null : request.headers["user-session-id"];
        return User.getFromSessionID(session);
    }

    static registerUser(username, password, sqlArgs, callback) {
        let connection = mysql.createConnection(sqlArgs);
        connection.connect((err) => {
            if (err) {
                console.warn("Database is not connected");
                callback(false);
                return;
            }

            let encPass = bcrypt.hashSync(password, hashSaltRounds);
            connection.query(fs.readFileSync(__basedir + "/sql/insert_user.sql").toString(), [username, encPass], (err, result) => {
                if (err) {
                    callback(false);
                    return;
                }

                callback(true);
            });
        });
    }

    static removeUser(user) {
        if (user) {
            user.kill();
            users.rem(user.getSessionID());
        }
    }
}

module.exports = User;