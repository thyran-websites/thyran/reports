const groups = require("../globals").groups;
const fs = require("fs");

const Report = require("./Report");
const DatabaseObject = require("./DatabaseObject");

class Group extends DatabaseObject {
    constructor(id) {
        super();

        this.id = id;
        this.name = null;

        this.reports = [];
    }

    onInit(self, connection, resolve, reject) {
        connection.query(fs.readFileSync(__basedir + "/sql/get_group.sql").toString(), [self.getID()], (err, result) => {
            if (err) reject(err);

            if (result.length > 0) {
                let group = result[0];

                self.setName(group.name);

                connection.query(fs.readFileSync(__basedir + "/sql/get_group_reports.sql").toString(), [self.getID()], (err, result) => {
                    if (err) reject(err);

                    result.forEach(row => {
                        self.addReport(Report.getFromID(row.report));
                    });

                    resolve();
                });
            } else
                reject("No rows were exported");
        });
    }

    onSave(self, connection, resolve, reject) {
        // TODO save group in database
        resolve();
    }

    setName(name) {
        this.name = name;
    }

    addReport(report) {
        if (!report) return;

        this.reports.push(report);
    }

    getID() { return this.id; }
    getName() { return this.name; }
    getReports() { return this.reports; }

    static getFromID(id) {
        return id ? groups.get(id) : null;
    }
}

module.exports = Group;