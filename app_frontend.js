const express = require("express");

const User = require("./classes/User");
const Report = require("./classes/Report");

module.exports = (() => {
    let app = express.Router();

    app.use(express.static(__basedir + "/websites/dist/frontend"));

    // TODO temporary, remove
    app.get("/test", (req, res) => {
        res.sendFile(__basedir + "/websites/dist/test.html");
    });

    app.get("/userConfig", (req, res) => {
        let user = User.getFromRequest(req);
        if (user)
            res.send(JSON.stringify(user.getConfig()));
        else
            res.sendStatus(403);
    });

    app.post("/isAuthorized", async (req, res) => {
        let user = User.getFromRequest(req);
        let report = Report.getFromID(req.body.report);

        if (!user)
            res.sendStatus(403);
        else if (!report)
            res.sendStatus(404);
        else if (user.isAuthorizedForReport(report))
            res.send(JSON.stringify({
                name: report.getName(),
                data: await user.getReportData(report)
            }));
        else
            res.sendStatus(423);
    });

    app.post("/changeReport", (req, res) => {
        let user = User.getFromRequest(req);
        let report = Report.getFromID(req.body.report);

        if (user && report) {
            user.setLastReport(report);
            res.sendStatus(204);
        } else if (!user)
            res.sendStatus(403);
        else
            res.sendStatus(404);
    });

    app.get("/getReports", (req, res) => {
        let user = User.getFromRequest(req);
        if (user)
            res.send(JSON.stringify(user.getAuthorizedReports().filter(r => r.isRunning()).map(r => {
                return {
                    id: r.getID(),
                    name: r.getName(),
                    description: r.getDescription()
                }
            })));
        else
            res.sendStatus(403);
    });

    return app;
})();