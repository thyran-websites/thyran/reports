var gulp = require('gulp');
var sass = require('gulp-sass');

var cleanCSS = require("gulp-clean-css");
var cleanJS = require("gulp-minify");
var cleanHTML = require("gulp-htmlmin");

const sourceRoot = "./websites/src/";
const sourceDest = "./websites/dist/";

gulp.task("html", (callback) => {
  gulp
    .src(sourceRoot + "**/*.html")
    .pipe(cleanHTML({
      removeComments: true,
      collapseWhitespace: true,
    }))
    .pipe(gulp.dest(sourceDest));

  callback();
});

gulp.task("css", (callback) => {
  gulp
    .src([
      sourceRoot + "**/*.scss",
      "!" + sourceRoot + "**/vars.scss"
    ])
    .pipe(sass())
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(gulp.dest(sourceDest));

  callback();
});

gulp.task("js", (callback) => {
  gulp
    .src(sourceRoot + "**/*.js")
    .pipe(cleanJS({
      ext: {
        min: ".min.js"
      },
      noSource: true
    }))
    .pipe(gulp.dest(sourceDest));

  callback();
});

gulp.task("php", (callback) => {
  gulp
    .src(sourceRoot + "**/*.php")
    .pipe(gulp.dest(sourceDest));

  callback();
});

gulp.task("source", (callback) => {
  gulp.parallel("html", "css", "js", "php")();

  gulp.watch(sourceRoot + "**/*.html", gulp.series("html"));
  gulp.watch(sourceRoot + "**/*.scss", gulp.series("css"));
  gulp.watch(sourceRoot + "**/*.js", gulp.series("js"));
  gulp.watch(sourceRoot + "**/*.php", gulp.series("php"));

  callback();
});